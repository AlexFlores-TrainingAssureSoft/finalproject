import "bulma/css/bulma.css";
import { useState } from "react";

const Modal = () => {
  const[isModal, setIsModal] = useState(false);

  const handleClick = () => {
    setIsModal(!isModal);
  };

  const active = isModal ? "is-active" : "";
  return (
    <div>    
      <div className={`modal ${active}`}>
        <div className="modal-background" />
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Registro</p>
            <button
              onClick={handleClick}
              className="delete"
              aria-label="close"
            />
          </header>
          <section className="modal-card-body">
            <div className="field">
              <label className="label">Artista</label>
              <div className="control">
                <input
                  className="input"
                  type="text"
                  placeholder="Artista"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Album</label>
              <div className="control">
                <input
                  className="input"
                  type="text"
                  placeholder="Album"
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Año</label>
              <div className="control">
                <input
                  className="input"
                  type="text"
                  placeholder="Año"
                />
              </div>
            </div>
          </section>
          <footer className="modal-card-foot">
            <button className="button is-success">Guardar</button>
            <button onClick={handleClick} className="button">
              Cancelar
            </button>
          </footer>
        </div>
      </div>

      <button onClick={handleClick} className="button is-small is-info">
        Crear
      </button>
    </div>
  );
}

export default Modal;