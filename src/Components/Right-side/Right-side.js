import "bulma/css/bulma.css";
import './Right-side.css'
import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay, faPause, faMusic, faVideo, faImage, faComputer } from '@fortawesome/free-solid-svg-icons'

function RightSide({ checked, trackData,setOnPause }) {
    let songslist = [];
    const [album, setAlbum] = useState(['']);
    const [play, setPlay] = useState(true);
    useEffect(() => {
        if (checked) {

            songslist = JSON.parse(localStorage.getItem('artist')).filter(song => song.artist === checked);
            setAlbum(songslist);
        }
    }, [checked]);

    function isAlbum() {

        const [data] = album;
        const { artist, albums } = data;
        if (album == 0) {
            return <div className="container"><h1 className="title is-4 panel-heading"> Sin canciones</h1></div> 
        } else {

            return (
                albums.map((album, index) => {
                    return (
                        <div className="container card effectos" key={index}>
                            <h1 className='title is-4 panel-heading '>{artist}</h1>
                            <div className="container-group card-content">
                                <img className="album" src={album.imagen} />
                                <div className="detail-group">
                                    <div className="groupDetail card-content">
                                        <h2 className="subtitle is-5 come">{album.songName}</h2>
                                        <h2 className="subtitle is-5">Top Music</h2>
                                    </div>
                                    <div className="icons-container" id="some" onClick={() => setPlay(!play)}>
                                        {play ? <span className="icon fa-2xl" onClick={() => trackData(artist, index, album.albumName)}>
                                            <i className="icon-show"><FontAwesomeIcon icon={faPlay} /></i>
                                        </span> : <span className="icon fa-2xl">
                                            <i><FontAwesomeIcon icon={faPause} /></i>
                                        </span>}
                                    </div>
                                    <div className="time-detail card-content">
                                        <h2 className="subtitle is-6">{album.year}</h2>
                                        <h2 className="subtitle is-6">{album.time}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })
            )
        }
    }


    return (
        <div>
            {isAlbum()}
        </div>
    );
}

export default RightSide