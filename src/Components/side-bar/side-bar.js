import React, { useState } from "react";
import "./side-bar.css";
import "bulma/css/bulma.css";



const Sidebar = ({ checks }) => {

    const songslist = JSON.parse(localStorage.getItem('artist'))
    return (
        <>
            <div className="card boxer has-text-left has-background-white">
                <h1 className="menu-label title is-4 panel-heading has-text-grey-dark">
                    Artistas
                </h1>
                <aside className="menu">
                    <ul className="menu-list">
                        {songslist.map((item, index) => {
                            return (
                                <li  onClick={()=> checks(item.artist)} key={index}> 
                                <a className="has-text-grey-dark"> 
                                <figure className="image is-48x48 "> 
                                <img className="is-rounded" src={item.albums[0].imagen}></img> 
                                </figure> 
                                {item.artist} 
                                </a> 
                            </li> 
                            )
                        })}
                    </ul>
                </aside>
            </div>
        </>
    );
};

                    
                

export default Sidebar;
