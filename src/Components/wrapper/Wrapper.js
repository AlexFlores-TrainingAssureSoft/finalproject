import React from 'react'
import { useState } from 'react'
import Sidebar from '../side-bar/side-bar'
import RightSide from '../Right-side/Right-side'
import TopBar from '../top-bar/TopBar'
import Player from '../player/player'
import { musicTracks } from "../../constants/data"

export const Wrapper = () => {

  const [checkArtist, setCheckArstist] = useState("");
  const [currentSong, setCurrentSong] = useState("");
  const [onPause,setOnPause] = useState(false);
  const [trackInfo, setTrackInfo] = useState({
    artist: "",
    index: 0,
    album: ""
  })

  if(!JSON.parse(localStorage.getItem('artist'))){
    localStorage.setItem('artist',JSON.stringify(musicTracks));
  }

  function checks(data) {
    setCheckArstist(data)
  }


  const trackData = (artist, index, album) => {
    if (artist) { setTrackInfo({ artist, index, album }) }
  }

  return (
    <>
      <div className="columns is-multiline">
        <div className="column is-full">
          <Player trackInfo={trackInfo} onPause={onPause}/>
        </div>
        <div className="column is-full">
          <TopBar />
        </div>
        <div className="column is-one-third is-4-tablet is-12-mobile is-fullheight">
          <Sidebar checks={checks} />
        </div>
        <div className="column is-8-tablet is-12-mobile">
          <RightSide checked={checkArtist} trackData={trackData} setOnPause={setOnPause}/>
        </div>
      </div>
    </>
  )
}
