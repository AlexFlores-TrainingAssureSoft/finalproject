import { Formik, ErrorMessage  } from 'formik';
import * as yup from 'yup';
import { setLocale } from 'yup';

setLocale({
  mixed: {
    default: 'No es valido',
  },
  number: {
    min: 'Debe ser mayor a ${min}',
  },
  string: {
    min: 'Ingrese al menos ${min} caracteres'
  }
});

const FormSchema = yup.object({
  artist: yup.string()
    .label('nombre de artista')
    .required('Debes ingresar el ${label}.')
    .max(45)
    .min(3)
    .trim(),
  album: yup.string()
    .label('nombre del album')
    .required('Debes ingresar el ${label}.')
    .max(45)
    .min(3)
    .trim(),
  year: yup.string()
    .label('año')
    .required('Debes ingresar el ${label}.')
    .max(4)
    .min(4)
    .trim(),
  music: yup.string()
    .label('link de musica')
    .required('Debes ingresar el ${label}.')
    .max(45)
    .min(3)
    .trim(),
  profile: yup.string()
    .label('link de la imagen')
    .required('Debes ingresar el ${label}.')
    .max(45)
    .min(3)
    .trim(),
  songName: yup.string()
    .label('nombre de la cancion')
    .required('Debes ingresar el ${label}.')
    .max(45)
    .min(3)
    .trim()
});


const TraineeForm = ({save,handleClick}) => {
  // const { firstName, lastName, email, address, mobile, headTrainer, group, feedback, guid } = trainee;
  return(
    <Formik
    initialValues = {{artist: '', album: '', year: '',music:'',profile:'', songName:''}} 
    validationSchema = {FormSchema}
    enableReinitialize= {true}
    onSubmit= {(values, actions) => {
      actions.resetForm();
      save(values);
      handleClick();
    }}
    >
      {(props) => (  
        <form onSubmit={props.handleSubmit}>
          <section className="modal-card-body">
            <div className="field">
              <label className="label">Artista</label>
              <div className="control">
                <input
                  id="artist"
                  name="artist"
                  className="input"
                  type="text"
                  placeholder="Artista"
                  onChange={props.handleChange}
                  value={props.values.artist}
                  onBlur={props.handleBlur("artist")}
                />
                <p className='has-text-danger'>{props.errors.artist}</p>
              </div>
            </div>
            <div className="field">
              <label className="label">Album</label>
              <div className="control">
                <input
                  id="album"
                  name="album"
                  className="input"
                  type="text"
                  placeholder="Album"
                  onChange={props.handleChange}
                  value={props.values.album}
                  onBlur={props.handleBlur("album")}
                />
                <p className="has-text-danger">{props.errors.album}</p>
              </div>
            </div>
            <div className="field">
              <label className="label">Año</label>
              <div className="control">
                <input
                  id="year"
                  name="year"
                  className="input"
                  type="text"
                  placeholder="Año"
                  onChange={props.handleChange}
                  value={props.values.year}
                  onBlur={props.handleBlur("year")}
                />
                <p className="has-text-danger">{props.errors.year}</p>
              </div>
            </div>
            <div className="field">
              <label className="label">Nombre Cancion</label>
              <div className="control">
                <input
                  id="songName"
                  name="songName"
                  className="input"
                  type="text"
                  placeholder="Song Name"
                  onChange={props.handleChange}
                  value={props.values.songName}
                  onBlur={props.handleBlur("songName")}
                />
                <p className="has-text-danger">{props.errors.songName}</p>
              </div>
            </div>
            <div className="field">
              <label className="label">Music Link</label>
              <div className="control">
                <input
                  id="music"
                  name="music"
                  className="input"
                  type="text"
                  placeholder="Music link"
                  onChange={props.handleChange}
                  value={props.values.music}
                  onBlur={props.handleBlur("music")}
                />
                <p className="has-text-danger">{props.errors.music}</p>
              </div>
            </div>
            <div className="field">
              <label className="label">Profile Link</label>
              <div className="control">
                <input
                  id="profile"
                  name="profile"
                  className="input"
                  type="text"
                  placeholder="Profile link"
                  onChange={props.handleChange}
                  value={props.values.profile}
                  onBlur={props.handleBlur("profile")}
                />
                <p className="has-text-danger">{props.errors.profile}</p>
             </div>
            </div>
          </section>
          <footer className="modal-card-foot">
            <button type='submit' className="button is-success">Guardar</button>
          </footer>
        </form>
      )}
    </Formik>
  );
};

export default TraineeForm;