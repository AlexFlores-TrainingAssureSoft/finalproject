import "bulma/css/bulma.css";
import { useState } from "react";
import Form from "./Form";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faMusic, faVideo, faImage, faComputer } from '@fortawesome/free-solid-svg-icons'

const TopBar = () => {
  const[isModal, setIsModal] = useState(false);

  const handleClick = () => {
    setIsModal(!isModal);
  };

  const save = ({artist, album,year,songName, music, profile}) => {
    const data={
      artist:artist,
      albums:[
      {  
        songName: songName, albumName: album,
        imagen: profile,
        song: music, time: "04:00", year: "2009"
      }
      ]
    };

    const storageData = JSON.parse(localStorage.getItem('artist'));
    storageData.push(data);
    localStorage.setItem('artist',JSON.stringify(storageData));

    window.location.reload();

  }

  const renderModal = () => {
    const active = isModal ? "is-active" : "";
    return (
      <div className={`modal ${active}`}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">Registro</p>
              <button
                onClick={handleClick}
                className="delete"
                aria-label="close"
              />
            </header>
            <Form save={save} handleClick={handleClick}/>
          </div>
        </div>
    )
  }
 
  return (
    <div className="box buttons columns">
      {renderModal()}
      <div className="column one-third">
        <button className="button is-medium is-white">
          <span className="icon">
            <i><FontAwesomeIcon icon={faMusic} /></i>
          </span>
        </button>
        <button className="button is-medium is-white">
          <span className="icon">
            <i><FontAwesomeIcon icon={faVideo} /></i>
          </span>
        </button>
        <button className="button is-medium is-white">
          <span className="icon">
            <i><FontAwesomeIcon icon={faImage} /></i>
          </span>
        </button>
        <button className="button is-medium is-white">
          <span className="icon">
            <i><FontAwesomeIcon icon={faComputer} /></i>
          </span>
        </button>
      </div>
      <div className="column is-half">
      <button className="button is-medium is-white">
          Mi música
        </button>
        <button className="button is-medium is-white">
          Playlist
        </button>
        <button className="button is-medium is-white">
          Radio
        </button>
        <button className="button is-medium is-white">
          Store
        </button>
      </div>
      <div className="column one-third">
        <button onClick={handleClick} className="button is-medium is-light">
          Añadir
          <span className="icon ml-1">
            <i><FontAwesomeIcon icon={faPlus} /></i>
          </span>
        </button>
      </div>
    </div>
  );
}

export default TopBar;