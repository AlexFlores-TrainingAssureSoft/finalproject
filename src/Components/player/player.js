import { React, useState, useEffect } from "react";
import AudioPlayer from "react-h5-audio-player";
import './player.css';
import "react-h5-audio-player/lib/styles.css";
import 'bulma/css/bulma.min.css';

const Player = ({ trackInfo,onPause }) => {
  let musics = [];
  let data = [];
  const [trackIndex, setTrackIndex] = useState(0);
  const [albumInfo, setAlbumInfo] = useState([]);
  useEffect(() => {
    data = JSON.parse(localStorage.getItem('artist'));
    const datas = data.filter(artist => artist.artist === trackInfo.artist);
    if (trackInfo.artist) {
      musics = albumInfo;
      musics.push(datas[0].albums[trackInfo.index].song);
      setAlbumInfo(musics);
    }
  }, [trackInfo]);

  const handleClickPrevious = () => {
    setTrackIndex((currentTrack) =>
      currentTrack === 0 ? albumInfo[0].albums[0].src.length - 1 : currentTrack - 1
    );
  };

  const handleClickNext = () => {
    setTrackIndex((currentTrack) =>
      currentTrack < albumInfo.length - 1 ? currentTrack + 1 : 0
    );
  };
  return (
    <div className="App">
      <AudioPlayer
        autoPlay={true}
        layout="horizontal"
        src={albumInfo[trackIndex]}
        onPlay={!onPause}
        onPause={onPause}
        showSkipControls={true}
        showJumpControls={false}
        header={`Reproduciendo:`}
        footer="Breaking Code Team"
        onClickPrevious={handleClickPrevious}
        onClickNext={handleClickNext}
        onEnded={handleClickNext}
      />
    </div>
  );
}

export default Player;
