export const musicTracks = [
  {
    artist: "Ed Sheeran",
    albums: [
      { albumName: "=", songName: "Tides", imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F%3D.jpg?alt=media&token=65c90347-b7e1-4378-8a79-a4de1da1b51c", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2FEd%20Sheeran%2F%3D%2F01%20Tides.m4a?alt=media&token=0c2fab59-01f1-4285-a014-48db86c670c8", time: "04:00", year: "2021" },
      { albumName: "=", songName: "Shivers", imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F%3D.jpg?alt=media&token=65c90347-b7e1-4378-8a79-a4de1da1b51c", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2FEd%20Sheeran%2F%3D%2F02%20Shivers.m4a?alt=media&token=b11a44fe-25bb-40f3-b600-a7427dee05ff", time: "03:00", year: "2021" },
      { albumName: "=", songName: "First Times", imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F%3D.jpg?alt=media&token=65c90347-b7e1-4378-8a79-a4de1da1b51c", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2FEd%20Sheeran%2F%3D%2F03%20First%20Times.m4a?alt=media&token=2ac1b44f-095d-4bc3-b2c3-a4298e6afd62", time: "02:00", year: "2021" },
      { albumName: "=", songName: "Bad Habbits", imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F%3D.jpg?alt=media&token=65c90347-b7e1-4378-8a79-a4de1da1b51c", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2FEd%20Sheeran%2F%3D%2F04%20Bad%20Habits.m4a?alt=media&token=a181341b-9ab1-42f7-81b8-5afd9bd10aad", time: "01:00", year: "2021" },
      { songName: "Sing", imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fx.jpg?alt=media&token=77019e2e-23bb-4eab-83b5-4dc9e1f2ba7f", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2FEd%20Sheeran%2Fx%2F04%20Don't.m4a?alt=media&token=d0a892f9-c0ed-4219-8c96-42d6fefec199",albumName: "X", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2FEd%20Sheeran%2Fx%2F03%20Sing.m4a?alt=media&token=51105d66-eab4-4615-87c8-9ab9af3d8e73", time: "04:00", year: "2020" },
      { songName: "Don't", imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fx.jpg?alt=media&token=77019e2e-23bb-4eab-83b5-4dc9e1f2ba7f", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2FEd%20Sheeran%2Fx%2F04%20Don't.m4a?alt=media&token=d0a892f9-c0ed-4219-8c96-42d6fefec199",albumName: "X", time: "03:00", year: "2020" },
      { songName: "Afire Love", imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fx.jpg?alt=media&token=77019e2e-23bb-4eab-83b5-4dc9e1f2ba7f", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2FEd%20Sheeran%2Fx%2F04%20Don't.m4a?alt=media&token=d0a892f9-c0ed-4219-8c96-42d6fefec199",albumName: "X", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2FEd%20Sheeran%2Fx%2F12%20Afire%20Love.m4a?alt=media&token=e94f630b-9169-46cb-b049-5a58528ff0e2", time: "02:00", year: "2020" }
    ]
  },
  {
    artist: "Daddy Yankee",
    albums: [
      {
        songName: "Gasolina", albumName: "Barrio Fino",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fbarrio_fino.jpg?alt=media&token=1d68fd01-2fe4-48e6-b410-17b8b1baced9",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fdaddy%20yankee%2Fbarrio%20fino%2F02.%20Daddy%20Yankee%20-%20King%20Daddy_sample.mp3?alt=media&token=5d25f39e-b4dc-4756-bad0-9a4a795fb1da", time: "04:00", year: "2009"
      },
      {
        songName: "Lo que paso", albumName: "Barrio Fino",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fbarrio_fino.jpg?alt=media&token=1d68fd01-2fe4-48e6-b410-17b8b1baced9",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fdaddy%20yankee%2Fbarrio%20fino%2F05.%20Daddy%20Yankee%20-%20Gasolina_sample.mp3?alt=media&token=49f429ee-b010-4b96-8eeb-ecf94e0ce581", time: "03:00", year: "2009"
      },
      {
        songName: "Tu principe", albumName: "Barrio Fino",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fbarrio_fino.jpg?alt=media&token=1d68fd01-2fe4-48e6-b410-17b8b1baced9",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fdaddy%20yankee%2Fbarrio%20fino%2F08.%20Daddy%20Yankee%20-%20Lo%20Que%20Pas%C3%B3%2C%20Pas%C3%B3_sample.mp3?alt=media&token=5518f197-80d6-4eb8-bcd8-76820adda254", time: "02:00", year: "2009"
      },
      {
        songName: "Cuentame", albumName: "Barrio Fino",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fbarrio_fino.jpg?alt=media&token=1d68fd01-2fe4-48e6-b410-17b8b1baced9",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fdaddy%20yankee%2Fbarrio%20fino%2F10.%20Daddy%20Yankee%20-%20Cu%C3%A9ntame_sample.mp3?alt=media&token=3614c47f-1ccf-4863-b290-1e9d779cf6b4", time: "01:00", year: "2009"
      },
      {
        songName: "Cambio", albumName: "El Cartel",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fel_cartel.jpg?alt=media&token=1b7a89af-a3cd-499c-8d9f-f1dce653ac7d",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fdaddy%20yankee%2Fel%20cartel%2F03.%20Daddy%20Yankee%20-%20Cambio_sample.mp3?alt=media&token=f424b9f2-3ccc-45d8-9d42-e713acc680f6", time: "04:00", year: "2010"
      },
      {
        songName: "Impacto", albumName: "El Cartel",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fel_cartel.jpg?alt=media&token=1b7a89af-a3cd-499c-8d9f-f1dce653ac7d",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fdaddy%20yankee%2Fel%20cartel%2F05.%20Daddy%20Yankee%20-%20Impacto_sample.mp3?alt=media&token=865b8676-07fd-4219-9114-447faf74206e", time: "03:00", year: "2010"
      },
      {
        songName: "Ella me levanto", albumName: "El Cartel",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fel_cartel.jpg?alt=media&token=1b7a89af-a3cd-499c-8d9f-f1dce653ac7d",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fdaddy%20yankee%2Fel%20cartel%2F06.%20Daddy%20Yankee%20-%20Ella%20me%20levant%C3%B3_sample.mp3?alt=media&token=4c33f19d-cfb3-4d3b-b622-03a23ed06b3c", time: "02:00", year: "2010"
      }
    ]
  },

  {
    artist: "Eminem",
    albums: [
      {
        songName: "Get mine", albumName: "8 Mile",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F8_mile.jpg?alt=media&token=8b38b872-1313-4823-b980-870aca60e21a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Feminem%2F8%20mile%2F03-MC%20Breed%20%26%202Pac-Gotta%20Get%20Mine.mp3?alt=media&token=aa38e25b-4708-41e8-ba32-8bf183b11e77", time: "04:00", year: "2002"
      },
      {
        songName: "Get money", albumName: "8 Mile",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F8_mile.jpg?alt=media&token=8b38b872-1313-4823-b980-870aca60e21a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Feminem%2F8%20mile%2F06-Jr.%20Mafia-Get%20Money.mp3?alt=media&token=efd56fd1-450b-4d53-a5d5-fb7667bb0827", time: "03:00", year: "2002"
      },
      {
        songName: "Pain", albumName: "8 Mile",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F8_mile.jpg?alt=media&token=8b38b872-1313-4823-b980-870aca60e21a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Feminem%2F8%20mile%2F06-Jr.%20Mafia-Get%20Money.mp3?alt=media&token=efd56fd1-450b-4d53-a5d5-fb7667bb0827", time: "02:00", year: "2002"
      },
      {
        songName: "Runnin", albumName: "8 Mile",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F8_mile.jpg?alt=media&token=8b38b872-1313-4823-b980-870aca60e21a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Feminem%2F8%20mile%2F11-Pharcyde-Runnin'.mp3?alt=media&token=c314a33c-5816-4128-a582-0d5649aa51ec", time: "01:00", year: "2002"
      },
      {
        songName: "On fire", albumName: "Recovery",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Frecovery.jpg?alt=media&token=2c34798b-573a-450b-a706-bf3f4d37137c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Feminem%2FRecovery%2F03%20On%20Fire.mp3?alt=media&token=0e73e888-c6f9-4d9f-ba82-6868d9370d01", time: "04:00", year: "2004"
      },
      {
        songName: "Space bound", albumName: "Recovery",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Frecovery.jpg?alt=media&token=2c34798b-573a-450b-a706-bf3f4d37137c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Feminem%2FRecovery%2F10%20Space%20Bound.mp3?alt=media&token=48bd559d-3936-4a87-9b07-c8480a194a00", time: "03:00", year: "2004"
      },
      {
        songName: "Love the way you lie", albumName: "Recovery",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Frecovery.jpg?alt=media&token=2c34798b-573a-450b-a706-bf3f4d37137c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Feminem%2FRecovery%2F15%20Love%20the%20Way%20You%20Lie.mp3?alt=media&token=b7816c1b-8b3b-4a6f-9fb4-231104b8cf96", time: "02:00", year: "2004"
      },
      {
        songName: "You're never over", albumName: "Recovery",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Frecovery.jpg?alt=media&token=2c34798b-573a-450b-a706-bf3f4d37137c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Feminem%2FRecovery%2F16%20You're%20Never%20Over.mp3?alt=media&token=76134a62-a702-4986-a277-19371a2d6222", time: "01:00", year: "2004"
      }

    ]
  },
  {
    artist: "Green Day",
    albums: [{
      songName: "Jesus of suburbia", albumName: "American Idiot",
      imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Famerican_idiot.jpg?alt=media&token=797670a4-2169-47ce-930c-31367e6c3648",
      song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fgreenday%2Famerican%20idiot%2F02.%20Green%20Day%20-%20Jesus%20of%20Suburbia%20-%20I.%20Jesus%20of%20Suburbia%20-%20II.%20City%20of%20the%20Damned%20-%20III.%20I%20Don't%20Care%20-%20IV.%20Dearly%20Beloved%20-%20V.%20Tales%20of%20Another%20Broken%20Home_sample.mp3?alt=media&token=18a16213-4ba3-407e-b3db-0adccc574554", time: "04:00", year: "2008"
    },
    {
      songName: "Holiday", albumName: "American Idiot",
      imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Famerican_idiot.jpg?alt=media&token=797670a4-2169-47ce-930c-31367e6c3648",
      song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fgreenday%2Famerican%20idiot%2F03.%20Green%20Day%20-%20Holiday_sample.mp3?alt=media&token=3dc00b4a-1e8d-4df8-8721-0b4521a1458b", time: "03:00", year: "2008"
    },
    {
      songName: "Give me novacaine", albumName: "American Idiot",
      imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Famerican_idiot.jpg?alt=media&token=797670a4-2169-47ce-930c-31367e6c3648",
      song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fgreenday%2Famerican%20idiot%2F07.%20Green%20Day%20-%20Give%20Me%20Novacaine_sample.mp3?alt=media&token=558e3592-367f-4486-b5e5-a803f4792d68", time: "02:00", year: "2008"
    },
    {
      songName: "Extraordinary girl", albumName: "American Idiot",
      imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Famerican_idiot.jpg?alt=media&token=797670a4-2169-47ce-930c-31367e6c3648",
      song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fgreenday%2Famerican%20idiot%2F09.%20Green%20Day%20-%20Extraordinary%20Girl_sample.mp3?alt=media&token=fa3d2e28-a60b-439d-90f7-fb63bee5b083", time: "01:00", year: "2008"
    },
    {
      songName: "Having a blast", albumName: "Dookie",
      imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fdookie.jpg?alt=media&token=78de73fb-0d9a-4af5-9bf7-c6ebfcc7fce9",
      song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fgreenday%2Fdookie%2F02%20-%20Having%20a%20Blast.mp3?alt=media&token=0c7afdf1-098e-404b-83b5-5ecedb1934ff", time: "04:00", year: "2010"
    },
    {
      songName: "Chump", albumName: "Dookie",
      imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fdookie.jpg?alt=media&token=78de73fb-0d9a-4af5-9bf7-c6ebfcc7fce9",
      song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fgreenday%2Fdookie%2F03%20-%20Chump.mp3?alt=media&token=2d81bf5b-2748-4eb1-b4d2-2e0b711f7290", time: "03:00", year: "2010"
    },
    {
      songName: "Longview", albumName: "Dookie",
      imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fdookie.jpg?alt=media&token=78de73fb-0d9a-4af5-9bf7-c6ebfcc7fce9",
      song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fgreenday%2Fdookie%2F04%20-%20Longview.mp3?alt=media&token=2e453036-d246-4dd6-830a-bae0d5c65b73", time: "02:00", year: "2010"
    },
    {
      songName: "Welcome to paradise", albumName: "Dookie",
      imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fdookie.jpg?alt=media&token=78de73fb-0d9a-4af5-9bf7-c6ebfcc7fce9",
      song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fgreenday%2Fdookie%2F05%20-%20Welcome%20to%20Paradise.mp3?alt=media&token=a8515d1d-1ae7-43ba-800a-536bcf10f7d0", time: "01:00", year: "2010"
    }
    ]
  },
  {
    artist: "Harry Styles",
    albums: [
      {
        songName: "Golden", albumName: "Fine Line",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Ffine_line.jpg?alt=media&token=e7e74464-88df-4328-93c2-e39e4432cdda",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fharry%20styles%2Ffine%20line%2F01%20Golden.m4a?alt=media&token=d2627598-6948-4d81-a9a5-43e471ea132c", time: "04:00", year: "2019"
      },
      {
        songName: "Watermelon Sugar", albumName: "Fine Line",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Ffine_line.jpg?alt=media&token=e7e74464-88df-4328-93c2-e39e4432cdda",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fharry%20styles%2Ffine%20line%2F02%20Watermelon%20Sugar.m4a?alt=media&token=e291f0c3-c834-414e-8eeb-c3fa86c0c161", time: "03:00", year: "2019"
      },
      {
        songName: "Adore you", albumName: "Fine Line",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Ffine_line.jpg?alt=media&token=e7e74464-88df-4328-93c2-e39e4432cdda",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fharry%20styles%2Ffine%20line%2F03%20Adore%20You.m4a?alt=media&token=1b5e4379-bed4-48c3-991b-423b9c484171", time: "02:00", year: "2019"
      },
      {
        songName: "Lights up", albumName: "Fine Line",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Ffine_line.jpg?alt=media&token=e7e74464-88df-4328-93c2-e39e4432cdda",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fharry%20styles%2Ffine%20line%2F04%20Lights%20Up.m4a?alt=media&token=5aa86335-6a10-48ea-a00e-59bb044bd23a", time: "01:00", year: "2019"
      },
      {
        songName: "Late night", albumName: "Harries House",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fharries_house.jpg?alt=media&token=e1f43979-77b6-48ee-bf0b-36a52e46c10a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fharry%20styles%2Fharries%20house%2F02%20-%20Harry%20Styles%20-%20Late%20Night%20Talking.mp3?alt=media&token=11ec880d-497f-4b76-9285-2c18d3ed40f6", time: "04:00", year: "2021"
      },
      {
        songName: "At is was", albumName: "Harries House",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fharries_house.jpg?alt=media&token=e1f43979-77b6-48ee-bf0b-36a52e46c10a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fharry%20styles%2Fharries%20house%2F04%20-%20Harry%20Styles%20-%20As%20It%20Was.mp3?alt=media&token=aca054a4-974e-4a09-8a1d-210693ca4906", time: "03:00", year: "2021"
      },
      {
        songName: "Day Light", albumName: "Harries House",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fharries_house.jpg?alt=media&token=e1f43979-77b6-48ee-bf0b-36a52e46c10a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fharry%20styles%2Fharries%20house%2F05%20-%20Harry%20Styles%20-%20Daylight.mp3?alt=media&token=6123a210-ebbf-4d79-9709-ed0eabf99a6e", time: "02:00", year: "2021"
      },
      {
        songName: "Little freak", albumName: "Harries House",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fharries_house.jpg?alt=media&token=e1f43979-77b6-48ee-bf0b-36a52e46c10a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fharry%20styles%2Fharries%20house%2F06%20-%20Harry%20Styles%20-%20Little%20Freak.mp3?alt=media&token=9a9669b7-4bf3-4ea7-a72d-88aa749c18db", time: "01:00", year: "2021"
      }
    ]
  },
  {
    artist: "Linkin Park",
    albums: [
      {
        songName: "Don't Stay", albumName: "Meteora",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fmeteor.jpg?alt=media&token=8314676d-7002-46e8-9691-70209f36216c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Flinkin%20park%2Fmeteora%2F02%20-%20Don't%20Stay.mp3?alt=media&token=9801d2e1-5020-4d2d-a427-c4d68f1c6b44", time: "04:00", year: "2008"
      },
      {
        songName: "Lying from you", albumName: "Meteora",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fmeteor.jpg?alt=media&token=8314676d-7002-46e8-9691-70209f36216c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Flinkin%20park%2Fmeteora%2F04%20-%20Lying%20From%20You.mp3?alt=media&token=62be5a8d-98b3-4363-b60e-318e26c7414b", time: "03:00", year: "2008"
      },
      {
        songName: "Easier to run", albumName: "Meteora",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fmeteor.jpg?alt=media&token=8314676d-7002-46e8-9691-70209f36216c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Flinkin%20park%2Fmeteora%2F06%20-%20Easier%20To%20Run.mp3?alt=media&token=1de8c548-95be-4455-9ac7-8780d446812e", time: "02:00", year: "2008"
      },
      {
        songName: "Figure", albumName: "Meteora",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fmeteor.jpg?alt=media&token=8314676d-7002-46e8-9691-70209f36216c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Flinkin%20park%2Fmeteora%2F08%20-%20Figure.09.mp3?alt=media&token=10c3965d-78f9-431a-a167-619c17958c83", time: "01:00", year: "2008"
      },
      {
        songName: "Given to up", albumName: "Minutes to midnight",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fminutes_to_midnight.jpg?alt=media&token=9a666422-e9a3-4c59-88db-75773694fa4b",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Flinkin%20park%2Fminutes%20to%20midnight%2F02%20-%20Given%20Up.mp3?alt=media&token=4c7888d8-6215-4002-a409-e8eb24834670", time: "04:00", year: "2010"
      },
      {
        songName: "Leave out all the rest", albumName: "Minutes to midnight",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fminutes_to_midnight.jpg?alt=media&token=9a666422-e9a3-4c59-88db-75773694fa4b",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Flinkin%20park%2Fminutes%20to%20midnight%2F03%20-%20Leave%20Out%20All%20The%20Rest.mp3?alt=media&token=0ee401a1-3bdc-46e5-8515-86a2109c449b", time: "03:00", year: "2010"
      },
      {
        songName: "Bleed it out", albumName: "Minutes to midnight",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fminutes_to_midnight.jpg?alt=media&token=9a666422-e9a3-4c59-88db-75773694fa4b",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Flinkin%20park%2Fminutes%20to%20midnight%2F04%20-%20Bleed%20It%20Out.mp3?alt=media&token=b82e0768-48f9-4647-bf21-844ed775444d", time: "02:00", year: "2010"
      },
      {
        songName: "Valentine's day", albumName: "Minutes to midnight",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fminutes_to_midnight.jpg?alt=media&token=9a666422-e9a3-4c59-88db-75773694fa4b",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Flinkin%20park%2Fminutes%20to%20midnight%2F09%20-%20Valentine's%20Day.mp3?alt=media&token=6864cb82-f956-4608-a562-e403ba849e64", time: "01:00", year: "2010"
      }

    ]
  },
  {
    artist: "Mana",
    albums: [
      {
        songName: "Como un perro enloquecido", albumName: "Cuando los angeles lloran",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcuando_los_angeles_lloran.jpg?alt=media&token=4cf65adc-1f3c-43f0-aee0-a70102cd955a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmana%2Fcuando%20los%20angeles%20lloran%2F01.%20Mana%20-%20Como%20Un%20Perro%20Enloquecido_sample.mp3?alt=media&token=0a2c886e-0049-4417-acd0-58b01c6a0361", time: "04:00", year: "2008"
      },
      {
        songName: "Selva negra", albumName: "Cuando los angeles lloran",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcuando_los_angeles_lloran.jpg?alt=media&token=4cf65adc-1f3c-43f0-aee0-a70102cd955a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmana%2Fcuando%20los%20angeles%20lloran%2F02.%20Mana%20-%20Selva%20Negra_sample.mp3?alt=media&token=b58ff878-d824-402e-9dcc-e2b717377b63", time: "03:00", year: "2008"
      },
      {
        songName: "Hundido en un rincon", albumName: "Cuando los angeles lloran",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcuando_los_angeles_lloran.jpg?alt=media&token=4cf65adc-1f3c-43f0-aee0-a70102cd955a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmana%2Fcuando%20los%20angeles%20lloran%2F03.%20Mana%20-%20Hundido%20En%20Un%20Rinc%C3%B3n_sample.mp3?alt=media&token=9915d9cb-d6f1-41ff-aa5c-c3e37bc7f5f1", time: "02:00", year: "2008"
      },
      {
        songName: "Reloj cucu", albumName: "Cuando los angeles lloran",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcuando_los_angeles_lloran.jpg?alt=media&token=4cf65adc-1f3c-43f0-aee0-a70102cd955a",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmana%2Fcuando%20los%20angeles%20lloran%2F04.%20Mana%20-%20El%20Reloj%20Cucu_sample.mp3?alt=media&token=770f40f7-56c4-45d4-8bb0-ac5c24d59825", time: "01:00", year: "2008"
      }
    ]
  },
  {
    artist: "Maroon 5",
    albums: [
      {
        songName: "Makes me wonder", albumName: "Call and response",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcall_and_response.jpg?alt=media&token=d1fedb82-8364-4be7-b868-9db164592692",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmaroon%205%2Fcall%20and%20response%2F04.%20Maroon%205%20-%20Makes%20Me%20Wonder%20(Just%20Blaze%20remix)_sample.mp3?alt=media&token=4cb10b23-5aa4-4f34-97b3-fa3926d66889", time: "04:00", year: "2010"
      },
      {
        songName: "Wake up call", albumName: "Call and response",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcall_and_response.jpg?alt=media&token=d1fedb82-8364-4be7-b868-9db164592692",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmaroon%205%2Fcall%20and%20response%2F08.%20Maroon%205%3B%20David%20Banner%20-%20Wake%20Up%20Call%20(David%20Banner%20remix)_sample.mp3?alt=media&token=252e28ce-2620-4681-943a-4a78f771aaf4", time: "03:00", year: "2010"
      },
      {
        songName: "Harder to breathe", albumName: "Call and response",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcall_and_response.jpg?alt=media&token=d1fedb82-8364-4be7-b868-9db164592692",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmaroon%205%2Fcall%20and%20response%2F09.%20Maroon%205%3B%20The%20Cool%20Kids%20-%20Harder%20to%20Breathe%20(The%20Cool%20Kids%20remix)_sample.mp3?alt=media&token=523c2047-6165-4e49-9fae-5cd22e41cade", time: "02:00", year: "2010"
      },
      {
        songName: "This love", albumName: "Call and response",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcall_and_response.jpg?alt=media&token=d1fedb82-8364-4be7-b868-9db164592692",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmaroon%205%2Fcall%20and%20response%2F05.%20Maroon%205%20-%20This%20Love%20(C.%20_Tricky_%20Stewart%20remix)_sample.mp3?alt=media&token=0036d409-9865-473a-9a25-aed14c545939", time: "01:00", year: "2010"
      },
      {
        songName: "Payphone", albumName: "Overexposed",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Foverexposed.jpg?alt=media&token=79198906-8fe5-4d88-bf47-1fd33c794c03",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmaroon%205%2Foverexposed%2F02.%20Maroon%205%3B%20Wiz%20Khalifa%20-%20Payphone_sample.mp3?alt=media&token=11f0c7ab-bf99-46a6-be70-fb89b9341574", time: "04:00", year: "2012"
      },
      {
        songName: "Daylight", albumName: "Overexposed",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Foverexposed.jpg?alt=media&token=79198906-8fe5-4d88-bf47-1fd33c794c03",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmaroon%205%2Foverexposed%2F03.%20Maroon%205%20-%20Daylight_sample.mp3?alt=media&token=252bec0e-8230-475e-91f6-027cbee8093a", time: "03:00", year: "2012"
      },
      {
        songName: "Lucky", albumName: "Overexposed",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Foverexposed.jpg?alt=media&token=79198906-8fe5-4d88-bf47-1fd33c794c03",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmaroon%205%2Foverexposed%2F04.%20Maroon%205%20-%20Lucky%20Strike_sample.mp3?alt=media&token=a2181ec7-3928-40f0-b64b-92b96836365f", time: "02:00", year: "2012"
      },
      {
        songName: "The man who never lied", albumName: "Overexposed",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Foverexposed.jpg?alt=media&token=79198906-8fe5-4d88-bf47-1fd33c794c03",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fmaroon%205%2Foverexposed%2F05.%20Maroon%205%20-%20The%20Man%20Who%20Never%20Lied_sample.mp3?alt=media&token=eb2cda21-8934-449d-aaf7-afc557930c54", time: "01:00", year: "2012"
      }
    ]
  },
  {
    artist: "Ricky Martin",
    albums: [
      {
        songName: "Nobody wants to be lonely", albumName: "A medio vivir",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fa_medio_vivir.jpg?alt=media&token=4f47deab-3c72-4cde-842b-a2865fdc957f",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fricky%20martin%2Fnobody%2F01.%20Ricky%20Martin%20%26%20Christina%20Aguilera%20-%20Ricky%20Martin%20%26%20Christina%20Aguilera%20-%20Nobody%20Wants%20To%20Be%20Lonely_Duet%20Radio%20Edit_sample.mp3?alt=media&token=64d415cb-0de9-4b0c-b79b-f7fee92bb2c4", time: "04:00", year: "1998"
      },
      {
        songName: "Solo quiero amarte", albumName: "A medio vivir",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fa_medio_vivir.jpg?alt=media&token=4f47deab-3c72-4cde-842b-a2865fdc957f",
        song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fricky%20martin%2Fnobody%2F02.%20Ricky%20Martin%20%26%20Christina%20Aguilera%20-%20Ricky%20Martin%20%26%20Christina%20Aguilera%20-%20Solo%20Quiero%20Amarte%20(Nobody%20Wants%20To%20Be%20Lonely)_Radio%20Edit_sample.mp3?alt=media&token=5041bdfe-2b10-4dec-94f7-58ca3da78396", time: "03:00", year: "1998"
      },
      { songName: "La vida loca", albumName: "Vuelve", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fricky%20martin%2Fvuelve%2F01.%20Ricky%20Martin%20-%20Livin'%20la%20Vida%20Loca_sample.mp3?alt=media&token=41244555-f3c5-4446-ae12-75cad30a4e40", time: "04:00", year: "2000" },
      { songName: "La vida loca (spanish)", imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fvuelve.jpg?alt=media&token=fddb9f42-8f33-4ba3-a5de-d0853b7586c8", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fricky%20martin%2Fvuelve%2F11.%20Ricky%20Martin%20-%20Livin'%20la%20Vida%20Loca%20(Spanish%20version)_sample.mp3?alt=media&token=33744f58-e1e7-4138-abdf-c9005be9b6a8", time: "03:00", year: "2000" },
      { songName: "Maria", song: "https://firebasestorage.googleapis.com/v0/b/music-url.appspot.com/o/music%2Fricky%20martin%2Fvuelve%2F14.%20Ricky%20Martin%20-%20Maria%20(Spanglish%20radio%20edit)_sample.mp3?alt=media&token=bf7d4e09-2e6a-496e-90e4-7a078124049a", time: "02:00", year: "2000" }

    ]
  },
  {
    artist: "Ariana Grande",
    albums: [
      {
        songName: "Everytime", albumName: "Sweetener",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fsweetener.jpg?alt=media&token=6c8b4931-b6e9-4597-b9fb-7475c5a91a12",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/ariana_grande%2F08.%20everytime.mp3?alt=media&token=0757e09b-414f-4070-be8c-19a08a46acda", time: "04:00", year: "2018"
      },
      {
        songName: "Breathin", albumName: "Sweetener",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fsweetener.jpg?alt=media&token=6c8b4931-b6e9-4597-b9fb-7475c5a91a12",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/ariana_grande%2F09.%20breathin.mp3?alt=media&token=26fb2ed6-eaa5-4d77-9d79-c499bea5c0a0", time: "03:00", year: "2018"
      },
      {
        songName: "No tears left to cry", albumName: "Sweetener",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fsweetener.jpg?alt=media&token=6c8b4931-b6e9-4597-b9fb-7475c5a91a12",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/ariana_grande%2F10.%20no%20tears%20left%20to%20cry.mp3?alt=media&token=9f4c9ae3-3c5c-4ffc-8f38-61d908c02b06", time: "02:00", year: "2018"
      }
    ]
  },
  {
    artist: "Adele",
    albums: [
      {
        songName: "Strangers by nature", albumName: "30",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F30.jpg?alt=media&token=30e24238-de14-4e11-a5bc-1c90a6a009d9",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/adele%2F01%20Strangers%20By%20Nature.mp3?alt=media&token=3acf007d-36a8-477d-9da2-b41adc9dd395", time: "04:00", year: "2021"
      },
      {
        songName: "Easy on me", albumName: "30",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F30.jpg?alt=media&token=30e24238-de14-4e11-a5bc-1c90a6a009d9",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/adele%2F02%20Easy%20On%20Me.mp3?alt=media&token=9faf5334-6311-4f8f-9cec-46042e778850", time: "03:00", year: "2021"
      },
      {
        songName: "Love is a game", albumName: "30",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2F30.jpg?alt=media&token=30e24238-de14-4e11-a5bc-1c90a6a009d9",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/adele%2F12%20Love%20Is%20A%20Game.mp3?alt=media&token=887f12d7-af88-4ae6-a0af-44a5b7fc4589", time: "02:00", year: "2021"
      }
    ]
  },
  {
    artist: "Camila Cabello",
    albums: [
      {
        songName: "My oh my", albumName: "Romance",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fromance.jpg?alt=media&token=d58f5651-d76c-448e-bb15-464ce378ac03",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/camila_cabello%2F01%20-%20My%20Oh%20My.mp3?alt=media&token=b57dc29d-d02e-4897-a277-2701b4cf91de", time: "04:00", year: "2019"
      },
      {
        songName: "Havana", albumName: "Romance",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fromance.jpg?alt=media&token=d58f5651-d76c-448e-bb15-464ce378ac03",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/camila_cabello%2F02%20-%20Havana.mp3?alt=media&token=cf014279-c8ad-4aad-9192-b4784f2e798c", time: "03:00", year: "2019"
      },
      {
        songName: "Liar", albumName: "Romance",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fromance.jpg?alt=media&token=d58f5651-d76c-448e-bb15-464ce378ac03",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/camila_cabello%2F08%20-%20Liar.mp3?alt=media&token=a2fc7f02-8f78-4aab-b172-e57d599d9f53", time: "02:00", year: "2019"
      }
    ]
  },
  {
    artist: "Demi Lovato",
    albums: [
      {
        songName: "Confident", albumName: "Confident",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fconfident.jpg?alt=media&token=ac8231de-fb35-4bb4-8f25-09fe2900b5e0",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/demi_lovato%2F02.%20Demi%20Lovato%20-%20Cool%20For%20The%20Summer_sample.mp3?alt=media&token=15dd776d-1af7-46de-8d53-a2cdf88fa0c2", time: "04:00", year: "2017"
      },
      {
        songName: "Cool for the summer", albumName: "Confident",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fconfident.jpg?alt=media&token=ac8231de-fb35-4bb4-8f25-09fe2900b5e0",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/demi_lovato%2F02.%20Demi%20Lovato%20-%20Cool%20For%20The%20Summer_sample.mp3?alt=media&token=15dd776d-1af7-46de-8d53-a2cdf88fa0c2", time: "03:00", year: "2017"
      }
    ]
  },
  {
    artist: "Imagine Dragons",
    albums: [
      {
        songName: "Radioactive", albumName: "Night visions",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fnight_visions.jpg?alt=media&token=33f5251b-1380-49fc-abc7-5e753e042d11",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/imagine_dragons%2F01.%20Imagine%20Dragons%20-%20Radioactive_sample.mp3?alt=media&token=82485369-0496-438f-8f30-61ea54dac1d7", time: "04:00", year: "2012"
      },
      {
        songName: "On the top of the world", albumName: "Night visions",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fnight_visions.jpg?alt=media&token=33f5251b-1380-49fc-abc7-5e753e042d11",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/imagine_dragons%2F05.%20Imagine%20Dragons%20-%20On%20Top%20of%20the%20World_sample.mp3?alt=media&token=4cca37e2-3b27-4140-85cc-f50c74486f43", time: "03:00", year: "2012"
      }
    ]
  },
  {
    artist: "Lil Nas X",
    albums: [
      {
        songName: "Montero", albumName: "Montero",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fmontero.jpg?alt=media&token=f0f3686f-7d1d-4e3a-834c-ee49c665a167",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/lil_nas_x%2F01.%20MONTERO%20(Call%20Me%20By%20Your%20Name).mp3?alt=media&token=f21f8c9e-0947-4893-8fd4-df448dde08d4", time: "04:00", year: "2021"
      },
      {
        songName: "Dead right now", albumName: "Montero",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fmontero.jpg?alt=media&token=f0f3686f-7d1d-4e3a-834c-ee49c665a167",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/lil_nas_x%2F02.%20DEAD%20RIGHT%20NOW.mp3?alt=media&token=19c100df-b22e-466f-a384-33bf3c45f4ee", time: "03:00", year: "2021"
      },
      {
        songName: "Industry baby", albumName: "Montero",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fmontero.jpg?alt=media&token=f0f3686f-7d1d-4e3a-834c-ee49c665a167",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/lil_nas_x%2F03.%20INDUSTRY%20BABY.mp3?alt=media&token=596454b3-91fb-4331-9c9a-807870f00d29", time: "02:00", year: "2021"
      }
    ]
  },
  {
    artist: "Men at work",
    albums: [
      {
        songName: "Who can it be now", albumName: "Cargo",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcargo.jpg?alt=media&token=319d8eeb-efa0-40b8-b267-d12da93ffc63",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/men_at_work%2F01.%20Men%20at%20Work%20-%20Who%20Can%20It%20Be%20Now__sample.mp3?alt=media&token=a701a451-53ad-43a6-a8ef-f27032b3196f", time: "04:00", year: "1983"
      },
      {
        songName: "Down under", albumName: "Cargo",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcargo.jpg?alt=media&token=319d8eeb-efa0-40b8-b267-d12da93ffc63",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/men_at_work%2F02.%20Men%20at%20Work%20-%20Down%20Under_sample.mp3?alt=media&token=391da297-5971-4d98-aca8-49fa4a24af65", time: "03:00", year: "1983" 
      }
    ]
  },
  {
    artist: "Michael Bolton",
    albums: [
      {
        songName: "Said I loved you but I lied", albumName: "The one thing",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fthe_one_thing.jpg?alt=media&token=31d9136b-cff3-4c07-ad17-4fed5513eb65",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/michael_bolton%2F01.11.%20Michael%20Bolton%20-%20Said%20I%20Loved%20You...%20But%20I%20Lied_sample.mp3?alt=media&token=5663c84c-4394-4515-aa09-9b2bd15c1285", time: "04:00", year: "1993"
      },
      {
        songName: "When a man loves a woman", albumName: "The one thing",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fthe_one_thing.jpg?alt=media&token=31d9136b-cff3-4c07-ad17-4fed5513eb65",
        song: "When a man loves a woman", song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/michael_bolton%2F01.08.%20Michael%20Bolton%20-%20When%20A%20Man%20Loves%20A%20Woman_sample.mp3?alt=media&token=c9249520-62f5-4b69-b932-351436470652", time: "03:00", year: "1993"
      }
    ]
  },
  {
    artist: "Scorpions",
    albums: [
      {
        songName: "Wind of change", albumName: "Crazy world",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcrazy_world.jpg?alt=media&token=2feb62d5-3752-47b7-ae59-494ad06ddc6b",
        song:  "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/scorpions%2F27.%20Wind%20Of%20Change.mp3?alt=media&token=ac6528ab-732f-45ef-9d52-afa676f51102", time: "04:00", year: "1990"
      },
      {
        songName: "Still loving you", albumName: "Crazy world",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fcrazy_world.jpg?alt=media&token=2feb62d5-3752-47b7-ae59-494ad06ddc6b",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/scorpions%2F21.%20Still%20Loving%20You.mp3?alt=media&token=b7a3e183-20f9-4f43-a11b-f0408b967e40", time: "03:00", year: "1990"
      }
    ]
  },
  {
    artist: "Shawn Mendes",
    albums: [
      {
        songName: "I know what you did last summer", albumName: "Handwritten",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fhandwritten.jpg?alt=media&token=67491e8e-6e2e-4f1d-b37c-75299e99a79b",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/shawn_mendes%2FShawn%20Mendes%20-%20I%20know%20what%20you%20did%20last%20Summer.mp3?alt=media&token=e18fb17e-6dc3-4466-8e1c-2e851f2fab5f", time: "04:00", year: "2015"
      },
      {
        songName: "Stitches", albumName: "Handwritten",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fhandwritten.jpg?alt=media&token=67491e8e-6e2e-4f1d-b37c-75299e99a79b",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/shawn_mendes%2FShawn%20Mendes%20-%20Stitches.mp3?alt=media&token=a9c62744-0d00-4c15-8fa3-8530d9b4a111", time: "03:00", year: "2015"
      },
      {
        songName: "There is nothing holding me back", albumName: "Handwritten",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Fhandwritten.jpg?alt=media&token=67491e8e-6e2e-4f1d-b37c-75299e99a79b",
        song:  "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/shawn_mendes%2FShawn%20Mendes%20-%20There's%20nothing%20holdin'%20me%20back.mp3?alt=media&token=6e43e40b-bcf9-4dda-933a-85eb14be7985", time: "02:00", year: "2015"
      }
    ]
  },
  {
    artist: "Gustavo Cerati",
    albums: [
      {
        songName: "Bajan", albumName: "Amor amarillo",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Famor_amarillo.jpg?alt=media&token=d998f470-385e-49ae-9bff-f6422109f44c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/soda_stereo%2FGustavo%20Cerati%20-%206%20-%20Bajan.mp3?alt=media&token=1292159c-4485-41bd-b08f-a9f4b30dfdb0", time: "04:00", year: "1993"
      },
      {
        songName: "Lisa", albumName: "Amor amarillo",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Famor_amarillo.jpg?alt=media&token=d998f470-385e-49ae-9bff-f6422109f44c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/soda_stereo%2FGustavo%20Cerati%20-%203%20-%20Lisa.mp3?alt=media&token=eb91ad6a-d8b4-4910-943e-1016f0c7aa75", time: "03:00", year: "1993"
      },
      {
        songName: "Amor amarillo", albumName: "Amor amarillo",
        imagen: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/images_album%2Famor_amarillo.jpg?alt=media&token=d998f470-385e-49ae-9bff-f6422109f44c",
        song: "https://firebasestorage.googleapis.com/v0/b/music-fde36.appspot.com/o/soda_stereo%2FGustavo%20Cerati%20-%202%20-%20Amor%20amarillo.mp3?alt=media&token=e12c7f1b-37e8-488c-ad6f-6d554dd6332a", time: "02:00", year: "1993"
      }
    ]
  }
];