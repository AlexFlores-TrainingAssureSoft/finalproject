import RightSide from './Components/Right-side/Right-side';
import TopBar from './Components/top-bar/TopBar';
import Form  from './Components/top-bar/Form';
import Player  from './Components/player/player';
import './App.css';
import Sidebar from './Components/side-bar/side-bar';
import { Wrapper } from './Components/wrapper/Wrapper';
import 'bulma/css/bulma.min.css';

function App() {
  return (
    <div className="App">
      <Wrapper/>
    </div>
  );
}

export default App;
